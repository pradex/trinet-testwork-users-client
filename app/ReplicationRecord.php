<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplicationRecord extends Model
{
    protected $fillable = [
        'started_at',
        'status'
    ];
}
