<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'UsersController@index');
Route::group(['prefix' => 'ajax'], function () {
    Route::get('users', 'UsersController@all');
    Route::get('users/{email}', 'UsersController@show');
    Route::post('users', 'UsersController@store');
    Route::put('users', 'UsersController@update');
});
