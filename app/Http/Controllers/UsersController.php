<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use App\User;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UsersController extends Controller
{
    public function index()
    {
        return view('users_list');
    }

    public function all()
    {
        $data = User::orderBy('created_at', 'asc')->get();
        return new JsonResponse([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function store(StoreUser $request)
    {
        $request->validate();

        $user = new User($request->all());
        $user->created_at = new \DateTime();
        try{
            $user->save();
        }
        catch (\Exception $exception) {
            return new JsonResponse([
                'status' => 'fail',
                'data' => $exception->getMessage(),
            ], 500);
        }

        return new JsonResponse([
            'status' => 'success',
        ]);
    }

    public function update(UpdateUser $request)
    {

        $request->validate();

        $data = $request->all();
        $user = User::find($data['email']);
        \Log::debug($user);
        $user->email = $data['newEmail'];
        $user->login = $data['newLogin'];
        $user->updated_at = new \DateTime();
        try{
            $user->save();
        }
        catch (\Exception $exception) {
            return new JsonResponse([
                'status' => 'fail',
                'data' => $exception->getMessage(),
            ],500);
        }

        return new JsonResponse([
            'status' => 'success',
        ]);
    }
}
