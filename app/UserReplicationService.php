<?php


namespace App;


class UserReplicationService
{
    protected $fromTime;
    protected $startTime;
    protected $chunkSize;

    protected $client;

    public function __construct()
    {
        $this->client = new ApiClient();
    }

    /**
     * Метод возвращает QueryBuilder для получения пользователей, созданных до предыдущей репликации,
     * но изменённых после нее. Возвратит ошибку, в случае, если ранее репликации не производились.
     * @throws \Exception
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getUsersToUpdate()
    {
        if (! $this->fromTime){throw new \Exception('The fromTime field must be specified before.');}

        $alreadyUpdated = $this->client->sendFilter([
            'updated_from' => $this->fromTime,
            'updated_to' => $this->startTime,
        ]);

        $query = User::query();
        $query->whereNotIn('email', $alreadyUpdated);
        if ($this->fromTime){
            $query->where('created_at', '<', $this->fromTime);
            $query->where('updated_at', '>', $this->fromTime);
        }
        $query->where('updated_at', '<=', $this->startTime);
        return $query;
    }

    protected function getUsersToCreate()
    {
        $alreadyCreated = $this->client->sendFilter([
            'created_from' => $this->fromTime,
            'created_to' => $this->startTime,
        ]);

        $query = User::query();
        $query->whereNotIn('email', $alreadyCreated);
        if ($this->fromTime){
            $query->where('created_at', '>', $this->fromTime);
        }
        $query->where('created_at', '<=', $this->startTime);
        return $query;
    }

    /**
     * Возвращает время последней успешной репликации.
     * @return \DateTime |null
     */
    protected function getFromTime()
    {
        $lastSuccessfulReplication = ReplicationRecord::where(['status' => 'success'])->orderBy('id', 'desc')->first();
        return $lastSuccessfulReplication ? $lastSuccessfulReplication->started_at : null;
    }

    protected function update()
    {
        $query = $this->getUsersToUpdate();
        $i=1;
        do{
            // Пагинация для достижения компромисса межу размером HTTP-запроса и количеством запросов к БД.
            $paginator = $query->paginate($this->chunkSize, ['*'],'', $i++);
            $response = $this->client->sendUpdate($paginator->toArray()['data']);
            $result = json_decode($response->getBody());
            // Если при записи блока на сервере происходили ошибки, занесем информацию о них в журнал.
            if ($result->status === 'fail' && isset($result->data)){
                \Log::error('Errors occurred during replication: ' . $result->data);
            }
        }while($paginator->hasMorePages());
    }
    protected function create()
    {
        /*
         *  TODO: Чтобы не повторяться, можно вынести код работы с пагинаций в ApiClient.
         *  Но, с другой стороны, не хочется передавать логику работы с QueryBuilder в ApiClient.
        */
        $query = $this->getUsersToCreate();
        $i=1;
        do{
            $paginator = $query->paginate($this->chunkSize, ['*'],'', $i++);
            $response = $this->client->sendCreate($paginator->toArray()['data']);
            $result = json_decode($response->getBody());
            if ($result->status === 'fail' && isset($result->data)){
                \Log::error('Errors occurred during replication: ' . $result->data);
            }
        }while($paginator->hasMorePages());
    }

    public function replicate()
    {
        $this->startTime = new \DateTime();
        $this->chunkSize = env('REPLICATION_CHUNK_SIZE', 1);
        $this->fromTime = $this->getFromTime(); // Время предыдущей успешной репликации

        try {
            // Сначала отправляем измененных пользователей, но только в случае если репликации производились ранее.
            if ($this->fromTime) {
                $this->update();
            }
            // Затем созданных с момента последней репликации
            $this->create();
            // Если не выкидывались исключения создаем запись об успешной репликации
            ReplicationRecord::insert(['started_at' => $this->startTime, 'status' => 'success']);
        }
        catch (\Exception $exception){
            \Log::error('Errors occurred during replication: ' . $exception->getMessage());
            // Передаём исключение дальше по стеку
            throw new \Exception($exception->getMessage(), $exception->getCode(), $exception->getPrevious());
        }
    }
}
