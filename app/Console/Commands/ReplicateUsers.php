<?php

namespace App\Console\Commands;

use App\ReplicationRecord;
use App\User;
use App\UserReplicationService;
use Illuminate\Console\Command;

class ReplicateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:replicate-users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rep = new UserReplicationService();
        $rep->replicate();
    }
}
