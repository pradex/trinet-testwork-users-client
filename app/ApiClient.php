<?php


namespace App;


use GuzzleHttp\Client;

class ApiClient extends Client
{
    protected $uri;

    function __construct(array $config = [])
    {
        $this->uri = env('SERVER_API_URI');
        parent::__construct($config);
    }

    public function sendUpdate($users)
    {
        $result = $this->put($this->uri, [
            'json' => ['data' => $users],
        ]);
        return $result;
    }

    public function sendCreate($users)
    {
        $result = $this->post($this->uri, [
            'json' => ['data' => $users],
        ]);
        return $result;
    }

    public function sendFilter($arr)
    {
        $result = $this->post($this->uri.'/filter', [
            'json' => $arr,
        ]);

        return json_decode($result->getBody()->getContents());
    }
}
