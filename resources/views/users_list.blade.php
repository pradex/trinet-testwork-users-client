@extends('layout.app')
@section('content')
    <div id="app">
        <table class="table">
            <thead class="table-striped">
            <tr>
                <th scope="col">Email</th>
                <th scope="col">Login</th>
                <th scope="col">Создан</th>
                <th scope="col">Обновлён</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="user in users">
                <td>@{{ user.email }}</td>
                <td>@{{ user.login }}</td>
                <td>@{{ user.created_at }}</td>
                <td>@{{ user.updated_at }}</td>
                <td><a class="btn btn-default" @click="editUser" :data-email="user.email">Редактровать</a></td>
            </tr>
            </tbody>
        </table>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#new_user_modal">Новый</button>
        <div class="modal fade" id="new_user_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Создание пользователя</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input v-model="newUserForm.email" name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Login</label>
                                <input v-model="newUserForm.login" name="login" class="form-control" id="exampleInputPassword1"
                                       placeholder="Login">
                            </div>
                            <div v-if="newUserForm.errors" class="alert alert-danger">
                                <strong>Ошибка: </strong> @{{ newUserForm.errors }}.
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" @click="storeUser" tabindex="1" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="edit_user_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Редактировани пользователя</h4>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input v-model="editUserForm.newEmail" name="email" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Login</label>
                                <input v-model="editUserForm.newLogin" name="login" class="form-control" id="exampleInputPassword1"
                                       placeholder="Login">
                            </div>
                            <div v-if="editUserForm.errors" class="alert alert-danger">
                                <strong>Ошибка: </strong> @{{ editUserForm.errors }}.
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" @click="updateUser" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
