<html>
<head>
    <title>@yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('head-css')
</head>
<body>
<div class="container">
    @yield('content')
</div>
<script src="{{ asset('js/app.js')}}"></script>
@stack('body-js')
</body>
</html>
