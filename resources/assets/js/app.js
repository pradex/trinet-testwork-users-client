window.jQuery = require('jquery');
require("bootstrap-sass/assets/javascripts/bootstrap");
var Vue = require('vue/dist/vue.min');
let ajaxUrl = '/ajax';

var app = new Vue({
    el: '#app',
    data: {
        users: [],
        newUserForm: {
            email: '',
            login: '',
            errors: null,
        },
        editUserForm: {
            email: '',
            newEmail: '',
            newLogin: '',
            errors: null,
        }
    },
    mounted() {
        this.updateUsersList();
    },
    methods: {
        updateUsersList: function () {
            jQuery.ajax({
                'method': 'GET',
                'url': ajaxUrl+'/users'
            })
                .done((data)=>{
                    this.users = data.data;
                })
            ;
        },
        storeUser() {
            jQuery.ajax({
                'method': 'POST',
                'url': ajaxUrl+'/users',
                'data': {
                    email: this.newUserForm.email,
                    login: this.newUserForm.login,
                },
            })
                .done((data) => {
                    this.newUserForm.errors = null;
                    this.newUserForm.email = '';
                    this.newUserForm.login = '';
                    jQuery('#new_user_modal').modal('hide');
                })
                .fail((response) => {
                    if (response.status === 422) {
                        this.newUserForm.errors = response.responseJSON;
                    }
                })
                .always(()=>{this.updateUsersList();})
            ;
        },
        updateUser() {
            jQuery.ajax({
                'method': 'PUT',
                'url': ajaxUrl+'/users',
                'data': {
                    email: this.editUserForm.email,
                    newEmail: this.editUserForm.newEmail,
                    newLogin: this.editUserForm.newLogin,
                },
            })
                .done((data) => {
                    this.editUserForm.errors = null;
                    jQuery('#edit_user_modal').modal('hide');
                })
                .fail((response) => {
                    if (response.status === 422) {
                        this.editUserForm.errors = response.responseJSON;
                    }
                })
                .always(()=>{this.updateUsersList();})
            ;
        },
        editUser(e) {
            this.editUserForm.email = e.target.dataset.email;
            let user = this.users.find((el)=>{return el.email === this.editUserForm.email});
            this.editUserForm.newEmail = user.email;
            this.editUserForm.newLogin = user.login;
            jQuery('#edit_user_modal').modal();
        },
    }
});
